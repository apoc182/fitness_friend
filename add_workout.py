from utils import create_connection, ExerciseMeta, insert_new_workout

name = input("Please enter workout name: ")
cooldown = input("Please enter cooldown time: ")
counter = 1
exercises = []
cont = "y"

conn = create_connection()

while cont == "y":
    print("---")
    print("Exercise " + str(counter))
    print("---")

    exercise_id = input("Enter exercise ID: ")
    weight = input("Enter weight: ")
    reps = input("Enter reps: ")

    exercises.append(ExerciseMeta(exercise_id, weight, reps))
    cont = input("Enter 'y' to add more: ")
    counter += 1

insert_new_workout(conn, name, cooldown, exercises)



    
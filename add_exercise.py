from utils import create_connection, insert_new_exercise, get_group_list

groups_list = get_group_list()

while True:
    print(groups_list)
    name = input("Enter name of exercise: ")
    link = input("Enter youtube link: ")
    description = input("Enter description: ")
    groups = [x for x in input("Enter comma delimited list of ids for the groups it works: ").split(",")]

    insert_new_exercise(name, description, link, groups)
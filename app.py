from telepot import Bot
from time import sleep
from telepot.loop import MessageLoop
from idle_statemachine import get_idle_statemachine

def is_command(message):
    text = message.get("text", False)
    if text:
        return text[0] == "/"
    return False

def is_callback_response(message):
    if message.get("data", False):
        return True
    return False

def get_command(message):
    return message.split(" ")[0][1:]

def get_callback_response(message):
    return message["data"]

def message_handler(message):
    
    user_id = message["from"]["id"]
    
    if user_id not in users:
        users[user_id] = get_idle_statemachine(bot, user_id)

    if is_command(message):
        command = get_command(message["text"])
        users[user_id].recieve_command(command)
        return
    
    if is_callback_response(message):
        response = get_callback_response(message)
        users[user_id].recieve_callback(response)
        return

    # If we get to here, it is but a humble message.
    print("Sending to the state machine")
    users[user_id].recieve_message(message["text"])


if __name__ == "__main__":
    bot = Bot('1329310903:AAHKnZAJG3niMx9e4VZIYH1bi54_ZXeE-ys')
    previous_update_id = 0
    users = {}
    MessageLoop(bot, message_handler).run_as_thread()

    while True:
        for key in users.keys():
            users[key].tick()
        sleep(1)

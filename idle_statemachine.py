from state_machine import StateMachine, State, ParentState
from workout_statemachine import get_workout_statemachine
from add_exercise_statemachine import get_add_exercise_statemachine


class Ready(State):
    def workout_command_recieved(self):
        if self.command == "workout" or self.command == "start":
            self.command = ""
            return True
        return False

    def add_exercise_command_recieved(self):
        if self.command == "addexercise":
            self.command = ""
            return True
        return False

class WorkoutParentState(ParentState):
    pass

class AddExerciseParentState(ParentState):
    pass

def get_idle_statemachine(bot, chat_id):

    state_machine = StateMachine(bot, chat_id)

    ready = Ready(state_machine, bot, chat_id)
    workout = WorkoutParentState(state_machine, bot, chat_id, get_workout_statemachine, "Workout")
    add_exercise = AddExerciseParentState(state_machine, bot, chat_id, get_add_exercise_statemachine, "Add exercise")

    state_machine.add_state(ready)
    
    def workout_cancelled():
        return workout.cancel

    def ex_cancelled():
        return add_exercise.cancel

    state_machine.add_transition(ready, workout, ready.workout_command_recieved)
    state_machine.add_transition(ready, add_exercise, ready.add_exercise_command_recieved)
    state_machine.add_transition(workout, ready, lambda: workout.state_machine.complete)
    state_machine.add_transition(workout, ready, workout_cancelled)
    state_machine.add_transition(add_exercise, ready, lambda: add_exercise.state_machine.complete)
    state_machine.add_transition(add_exercise, ready, ex_cancelled)

    return state_machine
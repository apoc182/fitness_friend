from utils import create_connection

conn = create_connection()

sql = "select id, name from exercises where link IS NULL;"

rows = conn.execute(sql).fetchall()
 
cur = conn.cursor()
for row in rows:
    link = input("Link for " + row[1])
    cur.execute(f"update exercises set link=? where id=?", (link, row[0]))
    conn.commit()
from utils import create_connection

conn = create_connection()

print("Please choose a workout:")

sql = "SELECT id, name, cooldown from workouts"

rows_workout = conn.execute(sql).fetchall()

for row in rows_workout:
    print(str(row[0]) + ") " + str(row[1]))

selection = str(input())

for row in rows_workout:
    if selection == str(row[0]):
        cooldown = str(row[2])
        break

sql = "SELECT name, link, desc, weight_kilogram, reps FROM workout_exercises we INNER JOIN exercises e ON e.id=we.exercise_id WHERE workout_id =" + selection

rows_exercise_workout = conn.execute(sql).fetchall()

for index, row in enumerate(rows_exercise_workout):
    print("Cooldown: " + cooldown)
    print("Exercise " + str(index + 1))
    print("Name: " + row[0])
    print("Link: " + row[1])
    print("Description: " + row[2])
    print("Weight: " + str(row[3]))
    print("Reps: " + str(row[4]))

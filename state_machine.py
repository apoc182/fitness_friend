


class State():
    def __init__(self, state_machine, bot, chat_id):
        self.bot = bot
        self.chat_id = chat_id
        self.callback = ""
        self.command = ""
        self.message = ""
        self.state_machine = state_machine

    def recieve_command(self, command):
        self.command = command

    def recieve_callback(self, callback):
        self.callback = callback

    def recieve_message(self, message):
        self.message = message

    def begin(self):
        pass

    def update(self):
        pass

    def end(self):
        pass

class ParentState(State):

    def __init__(self, state_machine, bot, chat_id, create_state_machine_method, state_machine_desc):
        State.__init__(self, state_machine, bot, chat_id)
        self.create_state_machine_method = create_state_machine_method
        self.state_machine_desc = state_machine_desc
        self.cancel = False

    def begin(self):
        self.state_machine = self.create_state_machine_method(self.bot, self.chat_id)
        self.cancel = False

    def update(self):
        self.state_machine.tick()

    def recieve_callback(self, callback):
        self.state_machine.recieve_callback(callback)

    def recieve_message(self, message):
        self.state_machine.recieve_message(message)

    def recieve_command(self, command):
        if command == "cancel":
            self.bot.sendMessage(self.chat_id, self.state_machine_desc + " cancelled.")
            self.cancel = True
        else:
            self.state_machine.recieve_command(command)

class GetMessage(State):
    """
    The callbacktoset in this is is a method this class will call the store the recieved message.
    """
    def __init__(self, state_machine, bot, chat_id, prompt, callback_to_set):
        State.__init__(self, state_machine, bot, chat_id)
        self.prompt = prompt
        self.callback_to_set = callback_to_set

    def begin(self):
        self.bot.sendMessage(self.chat_id, self.prompt)

    def message_recieved(self):
        if self.message:
            self.callback_to_set(self.message)
            self.message = ""
            return True
        return False


class StateMachine():

    def __init__(self, bot, chat_id):
        self.states = []
        self.bot = bot
        self.chat_id = chat_id
        self.transitions = {}
        self.current_state = None
        self.complete = False

    def recieve_command(self, command):
        self.current_state.recieve_command(command)

    def recieve_callback(self, callback_response):
        self.current_state.recieve_callback(callback_response)

    def recieve_message(self, message):
        print("recieved by state machine, sending to current state")
        self.current_state.recieve_message(message)
        
    def add_state(self, state):
        self.states.append(state)
        if not self.current_state:
            self.current_state = state
            self.current_state.begin()

    def add_transition(self, state_from, state_to, state_condition):
        if type(state_from) in self.transitions:
            self.transitions[type(state_from)].append(Transition(state_from, state_to, state_condition))
        else:
            self.transitions[type(state_from)] = [Transition(state_from, state_to, state_condition)]

    def tick(self):
        self.current_state.update()
        self.check_transitions()

    def check_transitions(self):
        for transition in self.transitions.get(type(self.current_state), []):
            if transition.state_condition():
                print("Condition: " + transition.state_condition.__name__ + " passed.")
                print("Transition to: " + str(type(transition.state_to)))
                print("---")
                self.current_state.end()
                self.current_state = transition.state_to
                self.current_state.begin()
                return True
        return False

class Transition():

    def __init__(self, state_from, state_to, state_condition):
        self.state_from = state_from
        self.state_to = state_to
        self.state_condition = state_condition

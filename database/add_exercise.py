from utils import create_connection, insert_new_exercise

conn = create_connection()

while True:
    name = input("Enter name of exercise: ")
    link = input("Enter youtube link: ")
    description = input("Enter description: ")

    insert_new_exercise(conn, name, description, link)
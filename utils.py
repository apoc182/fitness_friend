import sqlite3
from queue import Queue
from random import choice

DATABASE = "FitnessFriend.db"
DEFAULT_COOLDOWN = 30

def create_connection():
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(DATABASE)
    except Exception as e:
        print(e)

    return conn

def insert_new_exercise(name, description, link, groups):
    sql = "INSERT INTO exercises (name, link, desc) VALUES (?,?,?)"
    conn = create_connection()
    cur = conn.cursor()
    cur.execute(sql, (name, link, description))
    e_id = cur.lastrowid

    for g in groups:
        sql = f"INSERT INTO exercise_groups (exercise_id, group_id) VALUES ({e_id}, {g});"
        conn.execute(sql)
    conn.commit()

    return cur.lastrowid

def insert_new_workout(name, cooldown, exercises):
    sql_workout = "INSERT INTO workouts (name, cooldown) VALUES (?,?)"
    conn = create_connection()
    cur = conn.cursor()
    cur.execute(sql_workout, (name, cooldown))
    conn.commit()

    workout_id = cur.lastrowid

    sql_workout_exercises_prefix = "INSERT INTO workout_exercises (workout_id, exercise_id, weight_kilogram, reps) VALUES (?,?,?,?)"

    for exercise in exercises:
        cur.execute(sql_workout_exercises_prefix, (workout_id, exercise.id, exercise.weight, exercise.reps))
        conn.commit()

"""
Returns a queue of exercises for a workout.
"""
def get_workout_by_id(_id):
    sql = """

    SELECT e.id, e.name, e.link, e.desc, w.cooldown
    FROM workout_exercises we 
    INNER JOIN exercises e ON e.id=we.exercise_id 
    INNER JOIN workouts w ON we.workout_id=w.id 
    WHERE workout_id = ?
    
    """
    conn = create_connection()
    rows = conn.execute(sql, (str(_id))).fetchall()
    output = Queue()
    for row in rows:
        output.put(Exercise(row[0], row[1], row[2], row[4]))

    return output

def get_all_workouts():
    sql = "SELECT id, name from workouts"
    conn = create_connection()
    rows_workout = conn.execute(sql).fetchall()
    return [Workout(row[0], row[1]) for row in rows_workout]

def get_random_fullbody_workout():
    conn = create_connection()
    # Identify full body groups.
    sql = "select id from groups"
    ids = [row[0] for row in conn.execute(sql).fetchall()]
    sql = "select * from exercise_groups"
    rows = conn.execute(sql).fetchall()

    exercise_to_groups = {}

    for row in rows:
        if row[0] not in exercise_to_groups:
            exercise_to_groups[row[0]] = [row[1]]
        else:
            exercise_to_groups[row[0]].append(row[1])


    # Randomly select an exercise and note its groups.
    # While we do not have all the groups covered, keep going.
    exercise_ids = []
    while len(ids) > 0:
        random_exercise = choice(list(exercise_to_groups.keys()))
        contains_not_done_group = False
        for group in exercise_to_groups[random_exercise]:
            if group in ids:
                ids.remove(group)
                contains_not_done_group = True
        if contains_not_done_group:
            exercise_ids.append(random_exercise)
        del exercise_to_groups[random_exercise]
    
    def list_to_string(l):
        op = ""
        for e in l:
            op += str(e) + ", "
        return op[:-2]
    sql = "select id, name, link from exercises where id in (" + list_to_string(exercise_ids) + ");"
    rows = conn.execute(sql).fetchall()


    output = Queue()
    for row in rows:
        output.put(Exercise(row[0], row[1], row[2], DEFAULT_COOLDOWN))

    return output

def get_group_list():
    conn = create_connection()
    sql = "select * from groups;"
    rows = conn.execute(sql).fetchall()

    output = ""
    for row in rows:
        output += str(row[0]) + ") " + str(row[1]) + "\n"
    return output
    

class Workout():
    def __init__(self, _id, name):
        self._id = _id
        self.name = name

class Exercise():

    def __init__(self, _id, name, link, cooldown_time):
        self.id = _id
        self.name = name
        if not link:
            link = ""
        self.link = link
        self.cooldown_time = float(cooldown_time)

    def __str__(self):
        return self.name
from utils import create_connection

sql = input("Enter the table: ")

conn = create_connection()
rows = conn.execute("select * from " + sql).fetchall()

for row in rows:
    print(row)

from state_machine import StateMachine, GetMessage, State
from utils import get_group_list, insert_new_exercise



class AddExerciseStateMachine(StateMachine):
    def __init__(self, bot, chat_id):
        StateMachine.__init__(self, bot, chat_id)
        self.exercise_name = ""
        self.exercise_link = ""
        self.exercise_desc = ""
        self.exercise_groups = []
    
    def set_exercise_name(self, name):
        self.exercise_name = name
    
    def set_exercise_link(self, link):
        self.exercise_link = link

    def set_exercise_desc(self, desc):
        self.exercise_desc = desc

    def set_exercise_groups(self, groups):
        self.exercise_groups = [int(x) for x in groups.split(",")]

class GetName(GetMessage):
    pass

class GetLink(GetMessage):
    pass

class GetDescription(GetMessage):
    pass

class GetGroups(GetMessage):

    def begin(self):
        self.bot.sendMessage(self.chat_id, get_group_list())
        GetMessage.begin(self)


class Complete(State):
    def begin(self):
        insert_new_exercise(self.state_machine.exercise_name, self.state_machine.exercise_desc, self.state_machine.exercise_link, self.state_machine.exercise_groups)
        self.bot.sendMessage(self.chat_id, "Thanks! " + self.state_machine.exercise_name + " added to the database.")

        self.state_machine.complete = True


def get_add_exercise_statemachine(bot, chat_id):

    state_machine = AddExerciseStateMachine(bot, chat_id)

    get_name = GetName(state_machine, bot, chat_id, "Please enter an exercise name:", state_machine.set_exercise_name)
    get_link = GetLink(state_machine, bot, chat_id, "Please enter a YouTube link for this exercise:", state_machine.set_exercise_link)
    get_description = GetDescription(state_machine, bot, chat_id, "Please enter a description for this exercise:", state_machine.set_exercise_desc)
    get_groups = GetGroups(state_machine, bot, chat_id, "Please enter one or more muscle group ids from the above list. Comma delimited:", state_machine.set_exercise_groups)
    
    complete = Complete(state_machine, bot, chat_id)

    state_machine.add_state(get_name)
    state_machine.add_state(get_link)
    state_machine.add_state(get_description)
    state_machine.add_state(get_groups)
    state_machine.add_state(complete)

    state_machine.add_transition(get_name, get_link, get_name.message_recieved)
    state_machine.add_transition(get_link, get_description, get_link.message_recieved)
    state_machine.add_transition(get_description, get_groups, get_description.message_recieved)
    state_machine.add_transition(get_groups, complete, get_groups.message_recieved)

    return state_machine

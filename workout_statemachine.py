from state_machine import StateMachine, State
from utils import get_workout_by_id, get_all_workouts, create_connection, get_random_fullbody_workout
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from queue import Queue
from time import time
from datetime import datetime


def get_minutes_and_seconds(timedelta):
    seconds = timedelta.total_seconds()
    minutes = 0
    if seconds > 60:
        minutes = int(seconds / 60.0)
        seconds = round(seconds - (minutes * 60))
    return f"Workout time: {minutes} minutes and {seconds} seconds."



class GetExercise(State):
    def begin(self):

        buttons = []
        for workout in get_all_workouts():
            buttons.append(InlineKeyboardButton(text=workout.name, callback_data=workout._id))

        # Add a button for random workout.
        buttons.append(InlineKeyboardButton(text="Random Full Body", callback_data=-1))

        keyboard = InlineKeyboardMarkup(inline_keyboard=[buttons])

        self.bot.sendMessage(self.chat_id, 'Select a workout:', reply_markup=keyboard)

    def recieve_callback(self, callback):
        if callback:
            if callback == "-1":
                self.state_machine.exercises = get_random_fullbody_workout()
            else:
                self.state_machine.exercises = get_workout_by_id(callback)
            
            self.state_machine.total_exercises = self.state_machine.exercises.qsize()
            workout = ""
            for index, item in enumerate(list(self.state_machine.exercises.queue)):
                workout += str(index + 1) + ") " + item.name + "\n"
            self.bot.sendMessage(self.chat_id, workout)


class ShowExercise(State):

    def __init__(self, state_machine, bot, chat_id):
        State.__init__(self, state_machine, bot, chat_id)
        self.exercise_sent = False

    def begin(self):
        
        if not self.state_machine.exercises.empty(): 
            self.state_machine.exercise = self.state_machine.exercises.get()
            self.state_machine.exercise_count += 1
            message = self.state_machine.exercise.name + "\n\n" "Exercise " + str(self.state_machine.exercise_count) \
                      + "/" + str(self.state_machine.total_exercises) + "\n\n" + self.state_machine.exercise.link
            self.bot.sendMessage(self.chat_id, message)
            self.state_machine.cooldown_time = self.state_machine.exercise.cooldown_time
            self.exercise_sent = True
            self.state_machine.set_count = 0

class ShowSet(State):
    def __init__(self, state_machine, bot, chat_id):
        State.__init__(self, state_machine, bot, chat_id)
        self.done = False

    def begin(self):
        self.state_machine.set_count += 1
        self.done = False
        keyboard = InlineKeyboardMarkup(inline_keyboard=[[InlineKeyboardButton(text='Done', callback_data="dummy")]])
        set_text = "Set " + str(self.state_machine.set_count) if self.state_machine.set_count != 5 else "FINAL SET!!!"
        self.bot.sendMessage(self.chat_id, set_text, reply_markup=keyboard)
        self.callback = ""
    
    def recieve_callback(self, callback):
        self.done = True

class Wait(State):
    
    def begin(self):
        self.last_time = time()
        self.elapsed = 0
        self.bot.sendMessage(self.chat_id, "Take " + str(int(self.state_machine.cooldown_time)) + " there cobba. You've earnt it.")

    def update(self):
        self.elapsed += time() - self.last_time
        self.last_time = time()
    
    def time_complete(self):
        return self.elapsed >= self.state_machine.cooldown_time

class Complete(State):

    def begin(self):
        self.bot.sendMessage(self.chat_id, get_minutes_and_seconds(datetime.now() - self.state_machine.start_time))
        self.bot.sendMessage(self.chat_id, "Good work mate, you're done... for now.")
        self.state_machine.complete = True



class GetDifficulty(State):
    pass

class WorkoutStateMachine(StateMachine):
    def __init__(self, bot, chat_id):
        StateMachine.__init__(self, bot, chat_id)
        self.exercises = Queue()
        self.cooldown_time = None
        self.current_exercise = None
        self.set_count = 0
        self.exercise_count = 0
        self.total_exercises = 0
        self.start_time = datetime.now()


def get_workout_statemachine(bot, chat_id):

    state_machine = WorkoutStateMachine(bot, chat_id)

    get_exercise = GetExercise(state_machine, bot, chat_id)
    show_exercise = ShowExercise(state_machine, bot, chat_id)
    exercises_set = ShowSet(state_machine, bot, chat_id)
    wait = Wait(state_machine, bot, chat_id)
    complete = Complete(state_machine, bot, chat_id)

    state_machine.add_state(get_exercise)
    state_machine.add_state(show_exercise)
    state_machine.add_state(exercises_set)
    state_machine.add_state(wait)
    state_machine.add_state(complete)

    state_machine.add_transition(get_exercise, show_exercise, lambda: not state_machine.exercises.empty())
    state_machine.add_transition(show_exercise, exercises_set, lambda: show_exercise.exercise_sent)
    state_machine.add_transition(exercises_set, complete, lambda: exercises_set.done and state_machine.set_count == 5 and state_machine.total_exercises == state_machine.exercise_count)
    state_machine.add_transition(exercises_set, wait, lambda: exercises_set.done)
    state_machine.add_transition(wait, show_exercise, lambda: state_machine.set_count == 5 and wait.time_complete() )
    state_machine.add_transition(wait, exercises_set, wait.time_complete)

    return state_machine
